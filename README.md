Role Name
=========

A role that installs a basic KVM hypervisor.

Role Variables
--------------

The most important variables are listed below:

``` yaml
virtualization_pkg_state: present

virtualization_packages:
  - qemu-kvm
  - libvirt
  - bridge-utils
  - virt-install
  - cloud-utils
  - libguestfs-tools

virtualization_centos6_packages:
  - python-virtinst

virtualization_centos_netinst_url: "http://mi.mirror.garr.it/mirrors/CentOS/7/os/x86_64/"
virtualization_os_boot_dir: /var/lib/libvirt/boot
virtualization_os_boot_images:
  - "http://centos.mirror.garr.it/centos/7.7.1908/isos/x86_64/CentOS-7-x86_64-Minimal-1908.iso"
  - "http://releases.ubuntu.com/bionic/ubuntu-18.04.3-live-server-amd64.iso"
  - "https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2"
  - "https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img"
  - "https://cloud-images.ubuntu.com/bionic/current/focal-server-cloudimg-amd64.img"

virtualization_activate_forwarding: True

virtualization_disable_nfs: True
virtualization_nfs_services_to_be_disabled:
  - nfslock
  - rpcbind
  - gssproxy

virtualization_disable_iscsi: True
virtualization_iscsi_services_to_be_disabled:
  - iprupdate
  - iprinit
  - iprdump
  - iscsid

# Set this to false if ganeti is used for VM management
virtualization_enable_libvirtd: True
virtualization_services_to_be_enabled:
  - libvirtd

virtualization_sysctl_tuning:
  - { name: 'net.ipv4.ip_forward', value: '1', state: 'present' }

virtualization_kvm_create_lvm_pv: False
virtualization_kvm_create_lvm_vg: False
virtualization_kvm_lvm_pv:
  - /dev/fake_disk_1
virtualization_kvm_lvm_vg: vgxen

# Disable tuned on the host
centos_tuned_enabled: False
```

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
